$(document).ready(function(){

  $("#nav-mobile").hide();
  $(".nav-mobile-open").click(function(){
    $("#nav-mobile").toggle("slide");
  });
  $('.delete-notification').click(function(){
    $(this).fadeOut(100, function(){
      $(this).remove();
    });
  });

})
